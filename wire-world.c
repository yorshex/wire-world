#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

const int dirPatterns[8][2] = {
  {-1, 0},
  {-1, 1},
  { 0, 1},
  { 1, 1},
  { 1, 0},
  { 1,-1},
  { 0,-1},
  {-1,-1}
};

const char symbols[4] = {'.', '-', '*', '@'};

int height;
int width;
uint8_t* field;



int countElcHeadNeighbors(int x, int y, uint8_t* oldField) {
  int neighborsCount = 0;

  for (int i = 0; i < 8; i++) {
    int posX = x + dirPatterns[i][0];
    int posY = y + dirPatterns[i][1];

    if (posX > height) {
      posX -= height;
    }
    else if (posX < 0) {
      posX += height;
    }
    if (posY > width) {
      posY -= width;
    }
    else if (posY < 0) {
      posY += width;
    }

    if (oldField[posX * width + posY] == 3) {
      neighborsCount++;
    }
  }
  
  return neighborsCount;
}



void display(void) {
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      printf("%c", symbols[field[i * width + j]]);
    }
    printf("\n");
  }
}



void next(void) {
  uint8_t* oldField = malloc(height * width * sizeof(uint8_t));
  memcpy(oldField, field, height * width * sizeof(uint8_t));

  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      if (oldField[i * width + j] > 1) {
	field[i * width + j]--;
      }
      else if (oldField[i * width + j] == 1) {
	int elcHeadNeighborsCount = countElcHeadNeighbors(i, j, oldField);
	if (elcHeadNeighborsCount == 1 || elcHeadNeighborsCount == 2) {
	  field[i * width + j] = 3;
	}
      }
    }
  }

  free(oldField);
}



int main(void) {
  height = 5;
  width = 16;
  int updateCount = 100;
  field = malloc(height * width * sizeof(uint8_t));

  field[7 + width] = 1;
  field[8 + width] = 1;

  field[0 + 2 * width] = 1;
  field[1 + 2 * width] = 1;
  field[2 + 2 * width] = 3;
  field[3 + 2 * width] = 1;
  field[4 + 2 * width] = 1;
  field[5 + 2 * width] = 1;
  field[6 + 2 * width] = 1;
  field[7 + 2 * width] = 0;
  field[8 + 2 * width] = 1;
  field[9 + 2 * width] = 1;
  field[10 + 2 * width] = 1;
  field[11 + 2 * width] = 1;
  field[12 + 2 * width] = 1;
  field[13 + 2 * width] = 1;
  field[14 + 2 * width] = 1;
  field[15 + 2 * width] = 1;

  field[7 + 3 * width] = 1;
  field[8 + 3 * width] = 1;
  
  display();
  for (int i = 0; i < updateCount; i++) {
    usleep(100000);
    next();
    printf("\e[%dA", height);
    display();
  }
  
  return 0;
}
